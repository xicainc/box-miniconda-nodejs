# box-python-miniconda

[![wercker status](https://app.wercker.com/status/cfc48753a762945ceca88d87113846e9/m "wercker status")](https://app.wercker.com/project/bykey/cfc48753a762945ceca88d87113846e9)

## Objective

This is meant to be a 'box' for the Wercker CI system that will contain the Linux-based, x64 [Miniconda](http://conda.pydata.org/miniconda.html) with Python 2.7.

## Anaconda, Miniconda, and `conda`

[Miniconda](http://conda.pydata.org/miniconda.html) is the minimal version of the [Anaconda](http://docs.continuum.io/anaconda/) Python distro by [Continuum Analytics](http://www.continuum.io/), which, if I may quote the website is:

> ...a free collection of powerful packages for Python that enables large-scale data management, analysis, and visualization for Business Intelligence, Scientific Analysis, Engineering, Machine Learning, and more.

The reason I've created this box is due to [`conda`](http://conda.pydata.org/docs/), an incredible Python package/environment manager that makes installing 'pesky' packages like `NumPy` or `SciPy` a breeze. Anyone who ever tried to install such packages with `pip` will know what I mean.

## How to use

### Example

I've put together a very simple example-repo which can be found in <https://bitbucket.org/somada141/box-python-miniconda-test>. The repo is being built as an app on Wercker and can be found [here](https://app.wercker.com/#applications/53ac1ce1c824a1c83d007dad).

What this example does is use `conda create` to create (duh) a new python environment with Python 2.7 and the entire Anaconda distribution. Subsequently, it runs a Python file which import packages `NumPy`, `SciPy`, and `Pandas`, and prints their version (thus proving they are accessible).

For the sake of a quick-reference the contents of the `wercker.yml` file used in the aforementioned repo are:

```
box: somada141/python-miniconda@0.0.3
build:
  steps:
    - script:
        name: test general environment
        code: |
          echo "python version $(python --version) running"
          echo "conda version $(conda --version) running"
    - script:
        name: create and activate Python 2.7 virtual environment with the full Anaconda distro
        code: |
          conda create --yes -n py27 python=2.7 anaconda
          source activate py27
    - script:
        name: Test those Pesky Packages (NumPy)
        code: python test_pesky.py
```

**IMPORTANT!**: Please note the following:

- You do NOT need to install the whole Anaconda distro, its rather big and takes a while.
- During my test of this 'box' I did not manage to make it work without an environment. DO create one for your tests.
- You HAVE to `source activate` whatever environment you create after its creation. Otherwise the `PATH` will still be pointing to the `root` environment which won't have your fancy-smancy packages.

### Reading material
Please do take a look at the links below. They will show you how to create custom environments with `conda create` and how to activate them with `source activate`. In addition, you can see how to install whatever packages are available on the Continuum Analytics repos with `conda install`.

<http://conda.pydata.org/docs/intro.html>

<http://conda.pydata.org/docs/examples/create.html>

<http://conda.pydata.org/docs/examples/install.html>



# Changelog

## 0.0.3

- Working minimal version with conda support
- Verified that creating a custom Python environment works. An example repo can be found in <https://bitbucket.org/somada141/box-python-miniconda-test>

## 0.0.2

- First seemingly operational release. Used [suggestions by 'asmeurer' for installing conda on travis-ci](https://github.com/conda/conda/issues/700)

## 0.0.1

- Initial test release, wasn't working

# License

The MIT License (MIT)

Copyright (c) 2013 wercker

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.